
<div style="font-size: 20px; color: #FF6E68; align-items: center;">

    <h1 style="font-size: 40px;  ">Погода в городе <?= $userCity ?></h1>
    <p style="font-size: 20px;">Географические координаты: <?= $coordLat . ", " . $coordLon ?></p>
    <p style="font-size: 20px;">Температура: <?= $temp ?><img src="<?= $weatherIconUrl; ?> " alt="icon"> </p>
    <p>Влажность: <?= $humidity ?>%</p>
    <p>Давление: <?= $pressure ?> Ммрст</p>
    <p>Скорость ветра: <?= $windSpeed ?> м/c</p>

</div>
