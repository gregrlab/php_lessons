<?php

echo "<h1>Настоящие животные:</h1>";

$continents = [
    "Eurasia" => [
        "Neovison vison",
        "Ursus maritimus",
        "Prionailurus bengalensis",
        "Sciurus Linnaeus",
        "Mustela erminea",
    ],
    "Australia" => [
        "Macropus agilis",
        "Macropus eugenii",
        "Zaglossus bruijni",
        "Tachyglossus aculeatus",
        "Sarcophilus harrisii",
    ],
    "South America" => [
        "Procyon lotor",
        "Canis lupus beothucus",
        "Vulpes macrotis",
        "Lepus europaeus",
        "Odocoileus",

    ]
];

foreach ($continents as $continent => $animals){

    echo "<h2>" . $continent . "<h2>";
    echo implode(", ", $animals) . "<br>";

};

echo "<br><h2>Животные, в составе имени которых два слова</h2><br>";

$a = [];
$b = [];
$c = [];

foreach ($continents as $continent){

    foreach ($continent as $animals){

        if (substr_count($animals," ")===1){

            list($a[], $b[]) = explode(" ", $animals);
            echo $animals . "<br>";
        }
    }
}

shuffle($b);

foreach (array_keys($a)as $key){
    $c[] = $a[$key].' '.$b[$key];
}

$randomNameAnimals = implode(", ", $c);

echo "<h2>Животные с именами вперемешку</h2><br>";
echo $randomNameAnimals;


