<?php
$userName = 'Григорий';
$userSurename = 'Арабидзе';
$userEmail = 'gregory.arabidze@gmail.com';
$userHometown = 'Москва';
$userAbout = 'Менеджер продукта. Первым и основным языком программирования выбрал для себя php, для того чтобы непосредственно
 участвовать в разработке продукта над которым мы работаем в данный момент. В последствии хочу углубиться в MySQL.';
?>
<style>
tr, td {
padding: 5px 10px;
}
</style>
<body style="font-family: sans-serif; font-size: 15;">
<h1 style="size: 30"><strong>Страница пользователя <?= $userName ?><strong></h1>
<div>
    <table>
        <tr>
            <td>Имя </td>
            <td><?= $userName ?></td>
        </tr>
        <tr>
            <td>Фамилия </td>
            <td><?= $userSurename ?></td>
        </tr>
        <tr>
            <td>Email </td>
            <td><a href="mailto:gregory.arabidze@gmal.com"><?= $userEmail ?></a></td>
        </tr>
        <tr>
            <td>Город </td>
            <td><?= $userHometown ?></td>
        </tr>
        <tr>
            <td>О себе </td>
            <td><?= $userAbout ?></td>
        </tr>
    </table>
</div>
</body>